from time import perf_counter
import numpy as np
import matplotlib.pyplot as plt


def add_jumps(rand, reps, times, dt, lda, muj, sigmaj):
    '''returns the jump component for a diffusion process'''
    J = rand.poisson(lam=lda*dt, size=times*reps).astype(np.float64)
    np.add.accumulate(J, out=J)
    jumps = muj-sigmaj*sigmaj*.5 + rand.normal(size=int(J[-1]), scale=sigmaj)
    s = 0
    for i in range(times*reps): # slow as f***
        e = int(J[i])
        a = e-s
        if a==0:
            J[i] = 0
        elif a==1:
            J[i] = jumps[s]
        else:
            J[i] = sum(jumps[s:e]) #np.sum is better with LARGE arrays
        s = e
    return np.reshape(J, (times, reps))


def mc_jump(rand, reps, times, dt, lda, muj, sigmaj, s0):
    '''returns N trajs from a pure jump price process'''
    j = np.empty(shape=(times+1, reps))
    j[1:] = add_jumps(rand, reps, times, dt, lda, muj, sigmaj)
    np.add.accumulate(j[1:], out=j[1:], axis=0)
    np.exp(j[1:], out=j[1:])
    np.multiply(j[1:], s0, out=j[1:])
    j[0] = s0
    return j



def main():
    # params
    rand = np.random.RandomState(seed=2789)
    
    s0 = 40
    lda = 5
    muj = 0
    sigmaj = .2

    # pure jump process
    x = 300
    dt = 1/150
    r = 6
    a = mc_jump(rand, r, x, dt, lda, muj, sigmaj, s0)
    plt.figure()
    plt.plot(a, linewidth=.6)
    plt.title("Pure jump process")
    plt.show()

if __name__=="__main__":
    main()
