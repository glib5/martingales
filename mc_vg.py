import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter

#----------------------------------------------------------------------

##def mc_vg2(rand, v, dt, s0, theta, sigma, perf_counters, reps):
##    # slower than the previous
##    '''returns N trajectories from a variance-gamma process,
##    simulated as the difference of 2 gamma processes'''
##    x = np.empty(shape=(perf_counters+1, reps))
##    x[0] = 0
##    mu_p = .5*(np.sqrt(theta*theta + 2*sigma*sigma/v) + theta)
##    mu_q = mu_p - theta
##    dtv = dt/v
##    x[1:] = rand.gamma(shape=dtv, scale=mu_p*v, size=(perf_counters, reps)) - rand.gamma(shape=dtv, scale=mu_p*v, size=(perf_counters, reps))
##    np.add.accumulate(x[1:], out=x[1:], axis=0)
##    return x


##def mc_vg_mart(b, dt, s0, mu, sigma, perf_counters, reps, rand):
##    '''returns N trajectories from a variance-gamma process,
##    modified into a price process, simulated as a
##    perf_counter-changed brownian motion'''
##    x = np.zeros(shape=(perf_counters+1, reps), dtype=np.double)
##    # determine the dt that follow a gamma process
##    t = rand.gamma(shape=dt/b, scale=b, size=(perf_counters, reps))
##    g = rand.normal(size=(perf_counters, reps))
##    x[1:] = (mu -.5*sigma*sigma)*t + sigma*np.sqrt(t)*g
##    x = np.cumsum(x, axis=0)
##    return s0*np.exp(x)

def mc_vg(rand, v, dt, theta, sigma, periods, reps):
    '''returns N trajectories from a variance-gamma process,
    simulated as a perf_counter-changed brownian motion'''
    x = np.empty(shape=(periods+1, reps))
    x[0] = 0
    t = rand.gamma(shape=dt/v, scale=v, size=(periods, reps))
    x[1:] = theta*t + rand.normal(scale=sigma*np.sqrt(t))
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    return x


def mc_vg_S(rand, v, dt, s0, theta, sigma, periods, reps, r=0):
    '''returns N trajectories from a variance-gamma process,
    modified into a price process, simulated as a
    perf_counter-changed brownian motion'''
    ST = np.empty(shape=(periods+1, reps))
    ST = mc_vg(rand, v, dt, theta, sigma, periods, reps)
    T = np.arange(dt, periods*dt+dt, dt)
    ST[1:] += (T*np.log(1-v*(theta+sigma*sigma*.5))/v).reshape(periods, -1)
    np.exp(ST[1:], out=ST[1:])
    np.multiply(ST[1:], s0, out=ST[1:])
    ST[0]= s0
    if r!=.0:
        ST[1:] *= np.exp(T*r) # discounted price is martingale
    return ST

#----------------------------------------------------------------------

def MC(rand, s0, theta, v, sigma, dt, periods, reps, pr, MC_type=1):
    tim = perf_counter()
    ES = np.zeros(shape=periods+1)
    mc_err = np.zeros(shape=periods+1)
    print("%10d"%reps)
    for c in range(reps//pr):
        if MC_type==1:
            J = mc_vg(rand, v, dt, theta, sigma, periods, pr)
        elif MC_type==3:
            J = mc_vg_S(rand, v, dt, s0, theta, sigma, periods, pr)
        a = J
        ES += np.sum(a, axis=1)
        mc_err += np.sum(a*a, axis=1)
        i = (c+1)*pr
        if i%(1<<20)==0:
            print("%10d -- %2.2f sec\r"%(i, perf_counter()-tim), end='', flush=True)
    print("\n")    
    # collect stats
    ES /= reps
    mc_err = 3*np.sqrt( (mc_err/reps - ES*ES)/reps )
    return ES, mc_err



#######################################################################


def main():
    # params
    rand = np.random.RandomState(seed=7894)
    reps = 21#int(input("log_2 of reps? "))
    if reps<10:
        reps=10
    reps = 1<<reps
    pr = 1<<10 # partial reps

    periods = 12
    dt = .5
    ax = np.arange(periods+1)*dt
    
    sigma = .3
    theta = .0
    v = .1
    s0 = 4

    #--- MC
    
    # vg 
    ES, mc_err = MC(rand, s0, theta, v, sigma, dt, periods, reps, pr, MC_type=1)

    plt.subplot(2, 1, 1)
    plt.errorbar(x=ax, y=ES, yerr=mc_err)
    a = ES[0]
    b = .01
    plt.ylim(a-b, a+b)
    plt.plot([ax[0], ax[-1]], [a, a], "r")      

    # vg S
    ES, mc_err = MC(rand, s0, theta, v, sigma, dt, periods, reps, pr, MC_type=3)

    plt.subplot(2, 1, 2)
    plt.errorbar(x=ax, y=ES, yerr=mc_err)
    a = ES[0]
    plt.plot([ax[0], ax[-1]], [a, a], "r")
    plt.ylim(s0*.99, s0*1.01)

    plt.show()


if __name__=="__main__":
    main()













        
