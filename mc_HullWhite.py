from time import perf_counter as pc
from os import system, name

import numpy as np
import matplotlib.pyplot as plt
from numba import njit


@njit
def HW(times, reps, r0, th, a, dt, sigma, W):
    '''1 factor Hull-White model
    W, th and a are matrix of shape (times, reps)'''
    r = np.empty(shape=(times+1, reps))
    W *= sigma*np.sqrt(dt)
    r[0] = r0
    for t in range(times):
        r[t+1] = r[t] + (th[t] - a[t]*r[t])*dt + W[t]
    return r

#------------------------------------------------------------------------------

@njit
def mc_GCIR(reps, periods, v0, k, theta, dt, eta, G):
    '''returns N cir trajs'''
    V = np.empty(shape=(periods+1, reps))
    V[0] = v0
    for i in range(periods):
        v = V[i]
        g = G[i]
        V[i+1] = v + k*(theta - v)*dt + eta*np.sqrt(v*dt)*g
        V[i+1] = np.maximum(V[i+1], .0)
    return V

#------------------------------------------------------------------------------

def MC(rand, times, reps, p_reps, sigma, dt, r0, b, B, v0, k, theta, eta, a, th):
    fm = np.zeros(shape=times+1)
    sm = np.zeros(shape=times+1)
    tot = int(reps//p_reps)
    print('%20d'%(reps))
    tim = pc()
    for i in range(tot):
        # all gaussian shocks
        W = rand.normal(size=(2, times, p_reps)) 
        # classic, costant
        A = np.full(fill_value=a, shape=((times, p_reps))) 
        # time-dependent
        TH = mc_GCIR(p_reps, times, v0, k, theta, dt, eta, W[0]) 
        # 
        R = HW(times, p_reps, r0, TH, A, dt, sigma, W[1])
        fm += np.sum(R, axis=1)
        sm += np.sum(R*R, axis=1)
        c = (i+1)*p_reps
        if c%(1<<20)==0:
            print('%20d -- %2.2f'%(c, pc()-tim), end='\r', flush=True)
    print()
    ireps = 1/reps
    fm *= ireps
    sm = 3*np.sqrt( (sm*ireps - fm*fm)*ireps )
    return fm, sm, R



def __warmup():
    """numba warmup"""
    times = 2
    p_reps = 1
    a = .1
    times = 2
    dt = .5
    v0 = .1
    k = .1
    theta = .1
    eta = .1
    r0 = .1
    sigma = .1
    W = np.random.normal(size=(2, times, p_reps)) 
    A = np.full(fill_value=a, shape=((times, p_reps))) 
    TH = mc_GCIR(p_reps, times, v0, k, theta, dt, eta, W[0]) 
    R = HW(times, p_reps, r0, TH, A, dt, sigma, W[1])
    return


def main():
    __warmup()
##    if name=='nt':
##        _=system('cls')
    rand = np.random.RandomState(seed=4628)
    # mc params
    reps = 1<<int(input("log_2 of reps? "))
    p_reps = 1<<10
    if p_reps>reps:
        reps = p_reps
    # HW params
    r0 = 0.017
    b = .011
    B = .023
    # times params
    times = 60
    dt = 1/2
    sigma = .12
    # classic params
    a = .012
    th = .054
    # cir params
    v0 = .011
    k = .03
    eta = .14
    theta = .07
    # mc
    fm, sm, R = MC(rand, times, reps, p_reps, sigma, dt, r0, b, B, v0, k, theta, eta, a, th)
    #---
    plt.figure()
    ax = [i*dt for i in range(times+1)]
    plt.errorbar(x=ax, y=fm, yerr=sm, ecolor='red')

    plt.figure()
    plt.plot(ax, R, linewidth=.4)
    
    plt.show()

    return 0

if __name__=="__main__":
    main()
